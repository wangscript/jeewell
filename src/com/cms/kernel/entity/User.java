package com.cms.kernel.entity;

import javax.persistence.*;

import com.cms.kernel.util.DateTimeUtil;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * Copyright (C) 2013 2016 Well
 * 文件:User.java
 * 创建:Well
 * 日期:2016年5月25日 上午10:36:55
 * 来自:0.9.1
 * 版本:0.9.1
 * 描述:前台用户的实体类
 */

@Entity
@Table(name = "t_user")
public class User extends ABaseEntity
{

	@Column(name = "username")
	private String username; // 用户登录名称

	@Column(name = "password")
	private transient String password; // 用户登录密码
	
	@Transient // 在数据接的t_user表中，忽略这个字段，减少外键的约束
	private UserDetail userDetail; // 用户的详细信息，User类与UserDetail类是一对一的关系

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public UserDetail getUserDetail()
	{
		return userDetail;
	}

	public void setUserDetail(UserDetail userDetail)
	{
		this.userDetail = userDetail;
	}
	
}
