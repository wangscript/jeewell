package com.cms.kernel.util;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Copyright (C) 2013 2016 Well
 * 文件:DateTimeUtil.java
 * 创建:Well
 * 日期:2016年5月26日 下午2:56:34
 * 来自:
 * 版本:0.9.0
 * 描述:日期时间的util类
 * 		自定义 org.apache.commons.lang3.time.DateUtils和org.apache.commons.lang3.time.DateFormatUtils 中没有的方法
 * 		其它的一些日期时间的处理方法可以参考 org.apache.commons.lang3.time.DateUtils和org.apache.commons.lang3.time.DateFormatUtils 两个类
 */

public class DateTimeUtil
{
	private static Logger logger= LogManager.getLogger(DateTimeUtil.class);
	
	/**
	 * 日期时间的格式化
	 * 将日期时间转换为 yyyy-MM-dd HH:mm:ss
	 */
    public static final String DATE_TIME_FORMAT="yyyy-MM-dd HH:mm:ss";
    
    /**
     * 日期的格式化
     * 将日期转换为 yyyy-MM-dd
     */
    public static final String DATE_FORMAT="yyyy-MM-dd";
    
    /**
     * 时间的格式化
     * 将时间转换为 HH:mm:ss
     */
    public static final String TIME_FORMAT="HH:mm:ss";
    
    /**
     * 时区
     * 东八区，即北京时间
     */
    public static final String TIME_ZONE="GMT+8";
    
    /**
     * 将Date类型格式化成字符串类型，格式为 yyyy-MM-dd HH:mm:ss
     * @param date 
     * @return 字符串日期时间
     */
    public static String formatDateTime(Date date)
    {
    	return format(date,DATE_TIME_FORMAT);
    }
    
    /**
     * 将Date类型格式化成字符串类型，格式为 yyyy-MM-dd
     * @param date 
     * @return 字符串日期时间
     */
    public static String formatDate(Date date)
    {
    	return format(date,DATE_FORMAT);
    }
    
    /**
     * 将Date类型格式化成字符串类型，格式为 HH:mm:ss
     * @param date 
     * @return 字符串日期时间
     */
    public static String formatTime(Date date)
    {
    	return format(date,TIME_FORMAT);
    }
    
    /**
     * 将Date类型格式化成指定格式的字符串类型
     * @param date
     * @param format 格式
     * @return 字符串date
     */
    public static String format(Date date,String format)
    {
        String s=null;
        if(null!=date && StringUtils.isNotBlank(format))
        {
            s=DateFormatUtils.format(date,format);
        }
        else
        {
        	logger.error("参数date和参数format不能为null");
        }
        return s;
    }
    
    /**
     * 将字符串类型的日期时间转换为Date类型
     * @param s 字符串的日期时间
     * @param format 格式
     * @return Date类型的日期时间
     * @throws ParseException
     */
    public static Date parse(String s,String format) throws ParseException
    {
    	Date date=null;
    	if(StringUtils.isNoneBlank(s,format))
    	{
    		date=DateUtils.parseDate(s, format);
    	}
    	else
    	{
    		logger.error("参数s和参数format不能null");
    	}
    	return date;
    }
    
    /**
     * 判断年份是否为闰年
     * @param year 年份
     * @return	true---闰年，false---平年
     */
    public static boolean isLeapYear(int year)
    {
        boolean leapYear=((year%400)==0);
        if(!leapYear)
        {
            leapYear=((year%4)==0) && ((year%100)!=0);
        }
        return leapYear;
    }
	
}
