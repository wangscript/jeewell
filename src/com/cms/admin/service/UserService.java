package com.cms.admin.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.cms.common.mapper.UserMapper;
import com.cms.common.service.ABaseService;
import com.cms.kernel.entity.User;
import com.cms.kernel.entity.UserDetail;
import com.cms.kernel.util.IDUtil;

/**
 * Copyright (C) 2013 2016 Well
 * 文件:UserService.java
 * 创建:Well
 * 日期:2016年5月27日 上午9:48:33
 * 来自:
 * 版本:0.9.1
 * 描述:用户的service类
 * 		继承ABaseService抽象类
 */

@Service
public class UserService extends ABaseService<User>
{
	private static Logger logger=LogManager.getLogger(UserService.class);
	
	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private UserDetailService userDetailService;
	
	/**
	 * 查询用户列表
	 * 自定义级联查询，查询User类和UserDetail类
	 * @return
	 */
	public List<User> selectUserList()
	{
		return userMapper.selectUserList();
	}
	
	/**
	 * 新增用户
	 * 测试方法，里面的属性值是写死的
	 * 该方法不仅新增User类，同时也新增UserDetail类
	 * @return
	 */
	@Transactional
	public int insertUser()
	{
		int result=0;
		try
		{
			User user=new User();
			user.setId(IDUtil.generate());
			user.setUsername("1");
			user.setPassword("one");
			
			UserDetail userDetail=new UserDetail();
			userDetail.setId(IDUtil.generate());
			userDetail.setUserID(user.getId());
			userDetail.setNickname("一");
			userDetail.setGender(1);
			userDetail.setBirthday(new Date());
			
			result=super.insert(user);
			if(result>0)
			{
				result=userDetailService.insert(userDetail);
				if(result>0)
				{
					logger.info("新增user和userDetail成功");
				}
			}
			
		}
		catch(Exception e)
		{
			logger.error(e);
			/* 在捕捉异常的时候，进行事务的回滚 */
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return result;
	}
	
	/**
	 * 新增用户
	 * @param username 用户名称
	 * @param password 用户密码
	 * @return 新增用户的个数 
	 * 			=1---新增成功
	 * 			=0---新增失败
	 * @throws ParseException 
	 */
	@Transactional
	public int insertUser(String username,String password)
	{
		int result=0;
		try
		{
			if(StringUtils.isNoneBlank(username,password))
			{
				User user=new User();
				user.setUsername(username);
				user.setPassword(password);
				user.setInsertDateTime(new Date());
				user.setUpdateDateTime(new Date());
				user.setMemo("测试");
				result=super.insert(user);
			}
			else
			{
				logger.error("username 和  password 不能为空");
			}
		}
		catch(Exception e)
		{
			logger.error(e);
			/* 在捕捉异常的时候，进行事务的回滚 */
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return result;
	}
	
	/**
	 * 批量新增用户
	 * @return
	 */
	@Transactional
	public int insertUserList()
	{
		int result=0;
		List<User> userList=new ArrayList<User>();
		try
		{
			User user1=new User();
			user1.setId(IDUtil.generate());
			user1.setUsername("11");
			user1.setPassword("oneone");
			userList.add(user1);
			
			User user2=new User();
			user2.setId(IDUtil.generate());
			user2.setUsername("22");
			user2.setPassword("twotwo");
			userList.add(user2);
			
			User user3=new User();
			user3.setId(IDUtil.generate());
			user3.setUsername("33");
			user3.setPassword("threethree");
			userList.add(user3);
			
			result=super.insertList(userList);
			
		}
		catch(Exception e)
		{
			logger.error(e);
			/* 在捕捉异常的时候，进行事务的回滚 */
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return result;
	}
	
}
